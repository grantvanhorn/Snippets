var roleService = {

  harvester: {

    creep: null,

    setHarvesterMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'deliverEnergy';
    },

    run: function(creep) {
      this.creep = creep;

      this.setHarvesterMemory();

      if(creep.memory.currentTask == 'gatherEnergy')
        creep.harvestSource(creep.room.sources[creep.memory.sourceTarget]);

      if(creep.memory.currentTask == 'deliverEnergy')
        creep.depositeInNearestContainer(creep.room.containers[creep.memory.containerTarget]);
    }
  },

  transporter: {

    creep: null,

    setTransporterMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'deliverEnergy';
    },

    deliverEnergy: function() {
      let controller = Game.getObjectById('5836b84c8b8b9619519f1cc0');
      let tower = Game.getObjectById('58524426db8258503e671b3f');

      let extentions = [];
      this.creep.room.extentions.forEach((extention) => {
        if(extention.energy < extention.energyCapacity)
          extentions.push(extention);
      });
      // console.log("site", site);

      // TODO: make this the first check, if controller is counting down past 9999 fill it up
      if(this.creep.room.source[0].energy < this.creep.room.source[0].energyCapacity) {
        if(this.creep.transfer(this.creep.room.source[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          this.creep.moveTo(this.creep.room.source[0]);
        }
      } else if (extentions.length != 0) {
        this.creep.moveTo(extentions[0]);
        this.creep.transfer(extentions[0], RESOURCE_ENERGY)
      } else if(tower.energy < tower.energyCapacity) {
        this.creep.moveTo(tower);
        this.creep.transfer(tower, RESOURCE_ENERGY);
      } else if(this.creep.room.storage.store[RESOURCE_ENERGY] < 10000) {
        this.creep.moveTo(this.creep.room.storage);
        this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      } else {
        this.creep.transfer(controller, RESOURCE_ENERGY);
        this.creep.moveTo(controller);
      }
    },

    deliverEnergyToStorage: function() {
      this.creep.moveTo(this.creep.room.storage);
      this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
    },

    run: function(creep) {
      this.creep = creep;
      // creep.signController(creep.room.controller, "To Tenebria We Pray.");

      this.setTransporterMemory();

      if(creep.memory.currentTask == 'gatherEnergy'){
        creep.getFromContainer(creep.room.containers[creep.memory.containerTarget]);
      }

      if(creep.memory.currentTask == 'deliverEnergy') {
        this.deliverEnergyToStorage();
      }
    }
  },

  baseManager: {
    creep: null,

    setBaseManagerMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'manageBase';
    },

    getFromStorage: function() {
      this.creep.moveTo(this.creep.room.storage);
      this.creep.room.storage.transfer(this.creep, RESOURCE_ENERGY);
    },

    manageBase: function() {
      let controller = Game.rooms[this.creep.memory.homeRoom].controller;
      let tower = Game.rooms[this.creep.memory.homeRoom].towers[0];
      let tower2 = Game.rooms[this.creep.memory.homeRoom].towers[1];
      let extentions = [];
      this.creep.room.extentions.forEach((extention) => {
        if(extention.energy < extention.energyCapacity)
          extentions.push(extention);
      });
      let site = this.creep.room.find(FIND_CONSTRUCTION_SITES)[0];

      // TODO: make this the first check, if controller is counting down past 9999 fill it up
      if(this.creep.room.spawns[0].energy < this.creep.room.spawns[0].energyCapacity) {
        if(this.creep.transfer(this.creep.room.spawns[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          this.creep.moveTo(this.creep.room.spawns[0]);
        }
      } else if (extentions.length != 0) {
        let sortedExtentions = extentions.sort((a,b) => {
          return this.creep.pos.getRangeTo(a.pos) - this.creep.pos.getRangeTo(b.pos);
        });

        this.creep.moveTo(sortedExtentions[0]);
        this.creep.transfer(sortedExtentions[0], RESOURCE_ENERGY);
      } else if(tower && tower.energy <= 800) {
        this.creep.moveTo(tower);
        this.creep.transfer(tower, RESOURCE_ENERGY);
      }else if(tower2 && tower2.energy <= 1000) {
        this.creep.moveTo(tower2);
        this.creep.transfer(tower2, RESOURCE_ENERGY);
      } else if(this.creep.room.spawns[0].energy < this.creep.room.spawns[0].energyCapacity) {
        if(this.creep.transfer(this.creep.room.spawns[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          this.creep.moveTo(this.creep.room.spawns[0]);
        }
      }
    },

    run: function(creep) {
      this.creep = creep;

      this.setBaseManagerMemory();

      if(creep.memory.currentTask == 'gatherEnergy'){
        this.getFromStorage();
      }

      if(creep.memory.currentTask == 'manageBase') {
        this.manageBase();
      }
    }
  },

// -------------------------------------------------------------------------------------------------

  longDistanceHarvester: {

    creep: null,

    setHarvesterMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(_.sum(this.creep.carry) == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'deliverEnergy';
    },

    run: function(creep) {
      this.creep = creep;

      // this.setHarvesterMemory();

      if(creep.memory.currentTask == 'gatherEnergy')
        creep.mineLongDistance();

      if(creep.memory.currentTask == 'deliverEnergy')
        creep.depositeInHomeStorage();
    }
  },

// -------------------------------------------------------------------------------------------------

  generalWorker: {

    creep: null,

    // TODO : make a generic set harvester memory function based off role of the creep
    setHarvesterMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'deliverEnergy';
    },

    // Move to creep extention
    deliverEnergy: function() {
      let controller = this.creep.room.controller;
      // console.log('creep', this.creep.name);
      // console.log('towers', this.creep.room.towers[0]);
      let tower = this.creep.room.towers[0];

      let extentions = [];
      this.creep.room.extentions.forEach((extention) => {
        if(extention.energy < extention.energyCapacity)
          extentions.push(extention);
      });

      let site = this.creep.room.find(FIND_CONSTRUCTION_SITES)[0];

      if(this.creep.room.spawns[0].energy < this.creep.room.spawns[0].energyCapacity) {
        if(this.creep.transfer(this.creep.room.spawns[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          this.creep.moveTo(this.creep.room.spawns[0]);
        }
      } else if (extentions.length != 0) {
        if(this.creep.transfer(extentions[0], RESOURCE_ENERGY))
          this.creep.moveTo(extentions[0]);
      } else if(site) {
        this.creep.build(site);
        this.creep.moveTo(site);
      } else if(this.creep.room.storage && this.creep.room.storage.store[RESOURCE_ENERGY] < 5000) {
        this.creep.moveTo(this.creep.room.storage);
        this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      } else if(tower && tower.energy <= 800) {
        this.creep.moveTo(tower);
        this.creep.transfer(tower, RESOURCE_ENERGY);
      } else if(this.creep.room.storage && this.creep.room.storage.store[RESOURCE_ENERGY] < 15000) {
        this.creep.moveTo(this.creep.room.storage);
        this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      } else {
        this.creep.transfer(controller, RESOURCE_ENERGY);
        this.creep.moveTo(controller);
      }
    },

    /** @param {Creep} creep **/
    run: function(creep) {
      this.creep = creep;
      // creep.signController(creep.room.controller, "To Tenebria We Pray.");

      this.setHarvesterMemory();

      if(creep.memory.currentTask == 'gatherEnergy'){
        creep.harvestEnergy();
      }

      if(creep.memory.currentTask == 'deliverEnergy') {
        this.deliverEnergy();
      }
    }
  },

// -------------------------------------------------------------------------------------------------

  meleeAttacker: {
    creep: null,

    run: function(creep) {
      if(creep.room.name == creep.memory.homeRoom) {
        var exit = creep.room.findExitTo(creep.memory.targetRoom);
        creep.moveTo(creep.pos.findClosestByRange(exit));
      } else {
        let targetStruct = creep.room.find(FIND_HOSTILE_STRUCTURES)[0];
        let targetCreep = creep.room.find(FIND_HOSTILE_CREEPS)[0];

        let target = targetStruct ? targetStruct : targetCreep;

        if(creep.attack(target) == ERR_NOT_IN_RANGE) {
          creep.moveTo(target);
        }
      }
    }
  },

  // -------------------------------------------------------------------------------------------------
  // TODO : speedUpgrader and upgrader are the exact same code. Just a different body.
  // Make a decision on what to do. Are creeps with different bodies that provide the
  // same functionality the same type? I think I am convincing myself so.
  speedUpgrader: {
    creep: null,

    setUpgraderMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity && this.creep.room.storage.store[RESOURCE_ENERGY] > 15000)
        this.creep.memory.currentTask = 'upgradeController';
    },

    getFromStorage: function() {
      this.creep.moveTo(this.creep.room.storage);
      // this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      this.creep.room.storage.transfer(this.creep, RESOURCE_ENERGY);
    },

    upgradeController: function() {
      let controller = Game.rooms[this.creep.memory.homeRoom].controller;

      if(this.creep.transfer(controller, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
        this.creep.moveTo(controller);

      this.creep.transfer(controller, RESOURCE_ENERGY)
    },

    run: function(creep) {
      this.creep = creep;

      this.setUpgraderMemory();

      if(creep.memory.currentTask == 'gatherEnergy'){
        this.getFromStorage();
      }

      if(creep.memory.currentTask == 'upgradeController') {
        this.upgradeController();
      }
    }
  },

  upgrader: {
    creep: null,

    setUpgraderMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity && this.creep.room.storage.store[RESOURCE_ENERGY] > 15000)
        this.creep.memory.currentTask = 'upgradeController';
    },

    getFromStorage: function() {
      this.creep.moveTo(this.creep.room.storage);
      // this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      this.creep.room.storage.transfer(this.creep, RESOURCE_ENERGY);
    },

    upgradeController: function() {
      let controller = Game.rooms[this.creep.memory.homeRoom].controller;

      if(this.creep.transfer(controller, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
        this.creep.moveTo(controller);

      this.creep.transfer(controller, RESOURCE_ENERGY)
    },

    run: function(creep) {
      this.creep = creep;

      this.setUpgraderMemory();

      if(creep.memory.currentTask == 'gatherEnergy'){
        this.getFromStorage();
      }

      if(creep.memory.currentTask == 'upgradeController') {
        this.upgradeController();
      }
    }
  },

// -------------------------------------------------------------------------------------------------

  reserver: {
    run: function(creep) {
      creep.reserveRoomTargetController();
    }
  },

  claimer: {
    run: function(creep) {
      creep.claimRoomTargetController();
    }
  },

  tank: {
    setTankMemory: function(creep) {
      let rallyFlag = Game.flags['rally'];
      let warFlag = Game.flags['war'];

      if(rallyFlag) {
        creep.memory.currentTask = 'moveToRallyFlag';
        if(Game.flags['rally'].room && Game.flags['rally'].room.name == creep.room.name && 3 >= creep.pos.getRangeTo(rallyFlag))
          creep.memory.currentTask = 'wait';
      }

      if(warFlag) {
        if(creep.memory.currentTask == 'attackRoom'){
          return;
        }
        creep.memory.currentTask = 'moveToWarFlag';
        if(Game.flags['war'].room && Game.flags['war'].room.name == creep.room.name && 2 >= creep.pos.getRangeTo(warFlag))
          creep.memory.currentTask = 'attackRoom';
      }

    },
    run: function(creep) {
      this.setTankMemory(creep);

      if(creep.memory.currentTask == 'moveToRallyFlag')
        creep.moveToRallyFlag();
      if(creep.memory.currentTask == 'moveToWarFlag')
        creep.moveToWarFlag();
      if(creep.memory.currentTask == 'attackRoom')
        creep.attackRoom();
    }
  },

  builder: {
    creep: null,

    setBuilderMemory: function() {
      if(this.creep.carry.energy == 0)
        this.creep.memory.currentTask = 'gatherEnergy';
      if(this.creep.carry.energy == this.creep.carryCapacity)
        this.creep.memory.currentTask = 'build';
    },

    getFromStorage: function() {
      this.creep.moveTo(this.creep.room.storage);
      // this.creep.transfer(this.creep.room.storage, RESOURCE_ENERGY);
      this.creep.room.storage.transfer(this.creep, RESOURCE_ENERGY);
    },

    construct: function() {
      let site = this.creep.room.find(FIND_CONSTRUCTION_SITES)[0];

      this.creep.moveTo(site);
      this.creep.build(site);
    },

    run: function(creep) {
      this.creep = creep;
      // TODO : put this in memory

      this.setBuilderMemory();

      if(creep.memory.currentTask == 'gatherEnergy')
        this.getFromStorage();

      if(creep.memory.currentTask == 'build')
        this.construct();
    }
  },

  tower: {
    run: function(tower, room) {
      var durability = 20000;
      var damagedStructures = room.find(FIND_STRUCTURES, {
        filter: (structure) => {
          return (
            (structure.structureType == STRUCTURE_RAMPART && structure.hits < durability) ||
            (structure.structureType == STRUCTURE_WALL && structure.hits < durability) ||
            (structure.structureType == STRUCTURE_ROAD && structure.hits < structure.hitsMax) ||
            (structure.structureType == STRUCTURE_CONTAINER && structure.hits < structure.hitsMax) ||
            (structure.structureType == STRUCTURE_SPAWN && structure.hits < structure.hitsMax)
          );
        }
      });

      var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
      if (closestHostile) {
        tower.attack(closestHostile);
      } else if (damagedStructures.length > 0  && tower.energy > 800) {
        tower.repair(damagedStructures[0]);
      }

    }
  }
}

module.exports = roleService;
