Creep.prototype.sayName = function() {
    console.log(this.name);
}

Creep.prototype.sing = function() {
  this.say(Memory.creepSong[Memory.creepSongIndex], true);
}

Creep.prototype.harvestEnergy = function() {
  // TODO : FInd a better way to do this. Find the sources and pass them in.
  let sources = this.room.find(FIND_SOURCES);

  if(this.harvest(sources[this.memory.sourceTarget]) == ERR_NOT_IN_RANGE){
    this.moveTo(sources[this.memory.sourceTarget]);
  }
}

// Build nearings might be the wrong name for this. I think the game creates
// an array of construction sites and adds to it as you click. So FIND_CONSTRUCTION_SITES)[0]
// would be the first place you clicked, not the nearest site.
Creep.prototype.buildNearest = function() {
  let site = this.room.find(FIND_CONSTRUCTION_SITES)[0];

  this.moveTo(site);
  this.build(site);
}

// Not sure if doing this takes up memory or what.
// Creep.prototype.type = null;

Creep.prototype.depositeInNearestContainer = function(container) {
  this.moveTo(container);
  this.transfer(container, RESOURCE_ENERGY)
}

Creep.prototype.harvestSource = function(source) {
  if(this.harvest(source) == ERR_NOT_IN_RANGE){
    this.moveTo(source);
  }
}

Creep.prototype.getFromContainer = function(container) {
  let droppedEnergy = this.pos.findInRange(FIND_DROPPED_ENERGY, 3)[0];

  if(droppedEnergy) {
    console.log('droppedEnergyFound');
    if(this.pickup(droppedEnergy) == ERR_NOT_IN_RANGE)
      this.moveTo(droppedEnergy);
  } else {
    if(container.transfer(this, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
      this.moveTo(container);
  }
}

Creep.prototype.mineLongDistance = function() {
  let droppedEnergy = this.pos.findInRange(FIND_DROPPED_ENERGY, 2)[0];

  if(droppedEnergy) {
    console.log('droppedEnergyFound');
    if(this.pickup(droppedEnergy) == ERR_NOT_IN_RANGE)
      this.moveTo(droppedEnergy);

    return;
  }

  if(this.room.name == this.memory.targetRoom) {
    var source = this.room.find(FIND_SOURCES)[this.memory.sourceIndex];
    if(this.harvest(source) == ERR_NOT_IN_RANGE) {
      this.moveTo(source);
    }

  if(_.sum(this.carry) == this.carryCapacity)
    this.memory.currentTask = 'deliverEnergy';
  } else {
    if(!this.memory.targetRoomExit){
      this.memory.targetRoomExit = this.room.findExitTo(this.memory.targetRoom);
    }
    this.moveTo(this.pos.findClosestByRange(this.room.findExitTo(this.memory.targetRoom)));
  }
}

Creep.prototype.depositeInHomeStorage = function() {
  if(_.sum(this.carry) == 0){
    this.memory.currentTask = 'gatherEnergy';
    return;
  }

  if (this.room.name == this.memory.homeRoom) {
    this.moveTo(this.room.storage);
    this.transfer(this.room.storage, RESOURCE_ENERGY);
  } else {

    let site = this.room.find(FIND_CONSTRUCTION_SITES)[0];
    if(site) {
      if(this.build(site) == ERR_NOT_IN_RANGE)
        this.moveTo(site)
      return;
    }

    if(this.room.name == this.memory.targetRoom){
      var damagedRoads = this.pos.findInRange(FIND_STRUCTURES, 1, {
        filter: (structure) => {
          return ((structure.structureType == STRUCTURE_ROAD && structure.hits < structure.hitsMax));
        }
      });

      if(damagedRoads.length > 0) {
        if(this.repair(damagedRoads[0]) == ERR_NOT_IN_RANGE)
          this.moveTo(damagedRoads[0]);
        return;
      }
    }

    if(this.room.controller.my) {
      if(this.upgradeController(this.room.controller) == ERR_NOT_IN_RANGE)
        this.moveTo(this.room.controller);
      return;
    }

    if(!this.memory.homeRoomExit) {
      this.memory.homeRoomExit = this.room.findExitTo(this.memory.homeRoom);
    }
    this.moveTo(this.pos.findClosestByRange(this.memory.homeRoomExit));
  }
}

Creep.prototype.reserveRoomTargetController = function() {
  if(this.room.name == this.memory.homeRoom) {
    if(!this.memory.targetRoomExit) {
      this.memory.targetRoomExit = this.room.findExitTo(this.memory.targetRoom);
    }
    this.moveTo(this.pos.findClosestByRange(this.memory.targetRoomExit));
  } else {
    let targetController = Game.rooms[this.memory.targetRoom].controller;

    this.signController(targetController, "To Tenebria We Pray.");
    if(this.reserveController(targetController) == ERR_NOT_IN_RANGE){
      this.moveTo(targetController);
    }
  }
}


Creep.prototype.claimRoomTargetController = function() {
  if(this.room.name == this.memory.homeRoom) {
    if(!this.memory.targetRoomExit) {
      this.memory.targetRoomExit = this.room.findExitTo(this.memory.targetRoom);
    }
    this.moveTo(this.pos.findClosestByRange(this.memory.targetRoomExit));
  } else {
    let targetController = Game.rooms[this.memory.targetRoom].controller;

    if(this.claimController(targetController) == ERR_NOT_IN_RANGE){
      this.moveTo(targetController);
    }
  }
}

Creep.prototype.moveToRallyFlag = function() {
  let flag = Game.flags['rally'];

  if(this.pos != flag.pos)
    this.moveTo(flag);
}

Creep.prototype.moveToWarFlag = function() {
  let flag = Game.flags['war'];

  if(this.pos != flag.pos)
    this.moveTo(flag);
}

Creep.prototype.attackRoom = function() {
  let target = null;

  let sortedWalls = [];
  if(this.room.constructedWalls){
    sortedWalls = this.room.constructedWalls.sort((a,b) => {
      return this.pos.getRangeTo(a.pos) - this.pos.getRangeTo(b.pos);
    });
  }
  // let sortedWalls = [Game.getObjectById('58655eaa59d0245555ad6775')];
  // let hostileTowers = this.room.find(FIND_HOSTILE_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});

  let sortedHostileCreeps = [];
  if(this.room.hostileCreeps)
    sortedHostileCreeps = this.room.hostileCreeps.sort((a,b) => {
      return this.pos.getRangeTo(a.pos) - this.pos.getRangeTo(b.pos);
    });

  let sortedHostileStructures = [];
  if(this.room.hostileStructures)
    sortedHostileStructures = this.room.hostileStructures.sort((a,b) => {
      return this.pos.getRangeTo(a.pos) - this.pos.getRangeTo(b.pos);
    });

  let targetInstuction = 'creeps';

  if(targetInstuction == 'walls')
    target = sortedWalls[0];

  if(targetInstuction == 'creeps')
    target = sortedHostileCreeps[0];

  if(targetInstuction == 'structures')
    target = sortedHostileStructures[0];

  if(targetInstuction == 'spawns')
    target = this.room.hostileSpawns[0];

  if(targetInstuction == 'towers')
    target = this.room.hostileTowers[0];

  console.log('TARGET', target);

  if(this.attack(target) == ERR_NOT_IN_RANGE)
    this.moveTo(target);
}
