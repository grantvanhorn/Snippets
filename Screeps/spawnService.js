var spawnSpawnService = {
  harvester: function(spawn, homeRoom, sourceTarget, containerTarget, index) {
    spawn.createCreep(
      [
        WORK, WORK, WORK, WORK, WORK, WORK,
        CARRY,
        MOVE, MOVE
      ], undefined,
      {
        type: 'harvester',
        lvl: 4,
        homeRoom,
        sourceTarget,
        containerTarget,
        currentTask: null,
        index
      }
    );
  },

  builder: function(spawn, homeRoom, index) {
    spawn.createCreep(
      [
        WORK,  WORK,  WORK,  WORK,  WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE,  MOVE,  MOVE,  MOVE,  MOVE
      ], undefined,
      { type: 'builder', lvl: 4, homeRoom, index }
    );
  },

  transporter: function(spawn, homeRoom, containerTarget, index) {
    spawn.createCreep(
      [
        CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE
      ], undefined,
      { type: 'transporter', lvl: 4, containerTarget, homeRoom, currentTask: null, index }
    );
  },

  upgrader: function(spawn, homeRoom, index) {
    spawn.createCreep(
      [
        CARRY,
        MOVE, MOVE,
        WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK
      ], undefined,
      { type: 'upgrader', lvl: 4, currentTask: null, homeRoom, index }
    );
  },

  speedUpgrader: function(spawn, homeRoom, index) {
    spawn.createCreep(
      [
        CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE,
        WORK, WORK, WORK, WORK, WORK
      ], undefined,
      { type: 'speedUpgrader', lvl: 4, currentTask: null, homeRoom, index }
    );
  },

  baseManager: function(spawn, homeRoom, index, energyAvailable) {
    let creepBody = [];
    const iterations = Math.floor(energyAvailable / 150);

    for(let i = 0; i < iterations; i++) {
      creepBody.push(MOVE);
      creepBody.push(CARRY);
      creepBody.push(CARRY);

      if(i >= 13)
        break;
    }

    spawn.createCreep(
      creepBody,
      undefined,
      { type: 'baseManager',lvl: 4, homeRoom, index }
    );
  },

  // Cost : 1250
  longDistanceHarvester: function(spawn, sourceIndex, homeRoom, targetRoom, index) {
    spawn.createCreep(
      [
        WORK, WORK, WORK, WORK, WORK,
        CARRY, CARRY, CARRY, CARRY, CARRY,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ], undefined,
      {
        type: 'longDistanceHarvester',
        lvl: 4,
        sourceIndex,
        currentTask: null,
        homeRoom,
        targetRoom,
        index,
        currentTask: 'gatherEnergy'
      }
    );
  },

  reserver: function(spawn, homeRoom, targetRoom, index) {
    spawn.createCreep(
      [CLAIM, CLAIM, MOVE],
      undefined,
      {
        type: 'reserver',
        lvl: 4,
        currentTask: null,
        homeRoom,
        targetRoom,
        index
      }
    );
  },

  generalWorker: function(spawn, homeRoom, index, sourceTarget, energyAvailable) {
    let creepBody = [];
    const iterations = Math.floor(energyAvailable / 200);

    for(let i = 0; i < iterations; i++) {
      creepBody.push(MOVE);
      creepBody.push(CARRY);
      creepBody.push(WORK);
    }

    spawn.createCreep(
      creepBody,
      undefined,
      { type: 'generalWorker',lvl: 1, homeRoom, sourceTarget, index }
    );
  },

  meleeAttacker: function(spawn, homeRoom, targetRoom, index) {
    spawn.createCreep(
      [
        TOUGH, TOUGH,
        ATTACK, ATTACK, ATTACK, ATTACK,
        MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
      ],
      undefined,
      {
        type: 'meleeAttacker',
        lvl: 4,
        currentTask: null,
        homeRoom,
        targetRoom,
        index
      }
    );
  },

  tank: function(spawn, homeRoom, index) {
    spawn.createCreep(
      [
        MOVE,   MOVE,   MOVE,   MOVE,   MOVE,
        ATTACK, ATTACK, ATTACK, ATTACK, ATTACK
      ],
      undefined,
      {
        type: 'tank',
        lvl: 4,
        homeRoom,
        index
      }
    );
  },

  claimer: function(spawn, homeRoom, targetRoom, index) {
    spawn.createCreep(
      [CLAIM, CLAIM, MOVE],
      undefined,
      {
        type: 'claimer',
        lvl: 4,
        currentTask: null,
        homeRoom,
        targetRoom,
        index
      }
    );
  },
}

module.exports = spawnSpawnService;
