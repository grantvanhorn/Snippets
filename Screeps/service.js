var roleService = require('roleService');
var spawnService = require('spawnService');

var service = {
  roomConfigs: {
    E48S79: {
      spawn: null,
      lvl1: {},
      lvl4: {
        generalWorker: [],
        harvester: [null, null],
        transporter: [null, null, null, null],
        baseManager: [null],
        upgrader: [null],
        longDistanceHarvester: [null,      null,     null,     null,      null,     null],
        ldhTargetRoom:         ['E47S79', 'E47S79', 'E47S79', 'E47S79', 'E47S79', 'E47S79'],
        ldhIndex:              [ 0,        0,        0,        1,        1,        1],
        builder: [null],
        reserver: [null],
        reserverTarget: ['E47S79'],
        claimer: [],
        claimerTarget: [],
        meleeAttacker: [null],
        meleeAttackerTargetRoom: ['E47S79'],
        tank: [],
        speedUpgrader: []
      }
    },

// -------------------------------------------------------------------------------------------------

    E48S78: {
      spawn: null,
      lvl1: {
        generalWorker: [null, null],
        harvester: [],
        transporter: [],
        baseManager: [],
        upgrader: [],
        longDistanceHarvester: [],
        ldhTargetRoom:         [],
        ldhIndex:              [],
        builder: [],
        reserver: [],
        reserverTarget: [],
        claimer: [],
        claimerTarget: [],
        meleeAttacker: [],
        meleeAttackerTargetRoom: [],
        tank: []
      },
      lvl4: {
        generalWorker: [],
        harvester: [null, null],
        transporter: [null, null, 0, null],
        baseManager: [null, null],
        upgrader: [],
        longDistanceHarvester: [null,      null,     null,     null,     null,     null,     null,     null,     null,     null,     null,     null,     null],
        ldhTargetRoom:         ['E47S78', 'E47S78', 'E47S78', 'E47S78', 'E48S77', 'E48S77', 'E48S77', 'E48S77', 'E47S77', 'E47S77', 'E47S77', 'E47S77', 'E47S77'],
        ldhIndex:              [0,         0,        1,        1,        0,        0,        1,        1,        0,        0,        1,        1,        1],
        builder: [null],
        reserver: [],
        reserverTarget: [],
        claimer: [],
        claimerTarget: [],
        meleeAttacker: [null, null],
        meleeAttackerTargetRoom: ['E48S77', 'E47S78'],
        tank: [],
        speedUpgrader: [null, null, null, null, null, null]
      }
    },

// -------------------------------------------------------------------------------------------------

    E47S77: {
      spawn: null,
      lvl1: {
        generalWorker: [null, null],
        harvester: [],
        transporter: [],
        baseManager: [],
        upgrader: [],
        longDistanceHarvester: [],
        ldhTargetRoom:         [],
        ldhIndex:              [],
        builder: [],
        reserver: [],
        reserverTarget: [],
        claimer: [],
        claimerTarget: [],
        meleeAttacker: [],
        meleeAttackerTargetRoom: [],
        tank: [],
        speedUpgrader: []
      },
      lvl4: {
        generalWorker: [],
        harvester: [],
        transporter: [],
        baseManager: [],
        upgrader: [],
        longDistanceHarvester: [],
        ldhTargetRoom:         [],
        ldhIndex:              [],
        builder: [],
        reserver: [],
        reserverTarget: [],
        claimer: [],
        claimerTarget: [],
        meleeAttacker: [],
        meleeAttackerTargetRoom: [],
        tank: [],
        speedUpgrader: []
      }
    }
  },

  sortAndRunMyCreeps: function() {
    // TODO: Figure out how the fuck to make this not so fucking ugly
    let roomConfigs = JSON.parse(JSON.stringify(this.roomConfigs));

    for(let name in Game.creeps) {
      let creep = Game.creeps[name];
      // creep.sing();
      const homeRoom = creep.memory.homeRoom;
      const lvl = creep.memory.lvl;
      const type = creep.memory.type;
      const index = creep.memory.index;
      roomConfigs[homeRoom]['lvl'+lvl][type][index] = creep;

      roleService[type].run(creep);
    }

    this.spawnNewCreeps('E48S79', 4, roomConfigs);
    this.spawnNewCreeps('E48S78', 4, roomConfigs);
    this.spawnNewCreeps('E47S77', 1, roomConfigs);
  },

  spawnNewCreeps: function(roomName, lvl, roomConfigs) {
    console.log('SpawningNewCreeps in ' + roomName + ' at lvl' + lvl);

    if(roomConfigs[roomName].spawn == null) {
      roomConfigs[roomName].spawn = Game.rooms[roomName].find(FIND_MY_SPAWNS)[0];
    }

    roomConfigs[roomName]['lvl'+lvl].generalWorker.forEach((creep, index) => {
      if(creep == null){
        console.log('wanting to create a general worker in ', roomName);
        spawnService.generalWorker(roomConfigs[roomName].spawn, roomName, index, index % 2, Game.rooms[roomName].energyAvailable);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].harvester.forEach((creep, index) => {
      if(creep == null){
        console.log('wanting to create a harvester in ', roomName);
        spawnService.harvester(roomConfigs[roomName].spawn, roomName, index % 2, index % 2, index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].transporter.forEach((creep, index) => {
      if (creep == 0){
        return;
      }
      if(creep == null){
        console.log('wanting to create a transporter in ', roomName);
        spawnService.transporter(roomConfigs[roomName].spawn, roomName, index % 2, index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].baseManager.forEach((creep, index) => {
      if(creep == null && Game.rooms[roomName].energyAvailable >= 300){
        console.log('wanting to create a bm in ', roomName);
        spawnService.baseManager(roomConfigs[roomName].spawn, roomName, index, Game.rooms[roomName].energyAvailable);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].upgrader.forEach((creep, index) => {
      if(creep == null){
        console.log('wanting to create a upgrader in ', roomName);
        spawnService.upgrader(roomConfigs[roomName].spawn,roomName,index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].speedUpgrader.forEach((creep, index) => {
      if(creep == null){
        console.log('wanting to create a speedUpgrader in ', roomName);
        spawnService.speedUpgrader(roomConfigs[roomName].spawn,roomName,index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].longDistanceHarvester.forEach((creep, index) => {
      if(creep == null){
        console.log('wanting to create a longDistanceHarvester in ', roomName);
        spawnService.longDistanceHarvester(roomConfigs[roomName].spawn, roomConfigs[roomName]['lvl'+lvl].ldhIndex[index], roomName, roomConfigs[roomName]['lvl'+lvl].ldhTargetRoom[index], index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].builder.forEach((creep, index) => {
      if(creep == null && Game.rooms[roomName].constructionSites){
        console.log('wanting to create a builder in ', roomName);
        spawnService.builder(roomConfigs[roomName].spawn, roomName, index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].reserver.forEach((creep, index) => {
      if(creep == null) {
        console.log('wanting to create a reserver in ', roomName);
        spawnService.reserver(roomConfigs[roomName].spawn, roomName, roomConfigs[roomName]['lvl'+lvl].reserverTarget[index], index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].claimer.forEach((creep, index) => {
      if(creep == null) {
        console.log('wanting to create a claimer in ', roomName);
        spawnService.claimer(roomConfigs[roomName].spawn, roomName, roomConfigs[roomName]['lvl'+lvl].claimerTarget[index], index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].meleeAttacker.forEach((creep, index) => {
      if(creep == null) {
        console.log('wanting to create a maleeAttacker in ', roomName);
        spawnService.meleeAttacker(roomConfigs[roomName].spawn, roomName, roomConfigs[roomName]['lvl'+lvl].meleeAttackerTargetRoom[index], index);
      }
    });

    roomConfigs[roomName]['lvl'+lvl].tank.forEach((creep, index) => {
      if(creep == null) {
        console.log('wanting to create a tank in ', roomName);
        spawnService.tank(roomConfigs[roomName].spawn, roomName, index);
      }
    });
  },

  setUpRooms: function() {
    for (var roomName in Game.rooms) {
      console.log('##ROOM##: ' + roomName);
      var room = Game.rooms[roomName];
      if (Game.rooms[roomName].controller && Game.rooms[roomName].controller.my) {
        room.extentions = room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_EXTENSION}});
        room.towers = room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
        room.towers.forEach((tower) => roleService.tower.run(tower, room));
        room.sources = room.find(FIND_SOURCES);
        room.constructionSites = room.find(FIND_CONSTRUCTION_SITES)[0];
        room.spawns = room.find(FIND_MY_SPAWNS);
        room.containers = room.sources.map( source => {
          return source.pos.findInRange(FIND_STRUCTURES, 2, {
            filter: { structureType: STRUCTURE_CONTAINER}
          })[0];
        });
        room.damagedRoads = room.find(FIND_STRUCTURES, {
          filter: (structure) => {
            return ((structure.structureType == STRUCTURE_ROAD && structure.hits < structure.hitsMax));
          }
        });
      }

      room.hostileCreeps = room.find(FIND_HOSTILE_CREEPS, {filter: (creep) => {
        return(creep.owner.username != 'Hive_');
      }});

      // room.hostileCreeps = room.find(FIND_HOSTILE_CREEPS);
      room.hostileStructures = room.find(FIND_HOSTILE_STRUCTURES);
      room.constructedWalls = room.find(FIND_STRUCTURES, {
        filter: {structureType: STRUCTURE_WALL}
      });
      room.hostileSpawns = room.find(FIND_HOSTILE_SPAWNS);
      room.hostileTowers = room.find(FIND_HOSTILE_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
    }
  }
}

module.exports = service;
