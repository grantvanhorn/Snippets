import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import { register } from '../mutations';
import './Register.css';

const style = {
  marginLeft: 20,
};

class Register extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      email: ''
    };
  }

  onInputChage = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value
    });
  }

  register = async () => {
    console.log('this state', this.state);
    const user = await this.props.mutate({ variables: this.state });

    console.log('user', user);
  }

  render() {
    return (
      <div className="register-container">
        <Paper zDepth={2}>
          <Divider />
          <TextField
            hintText="Username"
            style={style}
            underlineShow={false}
            name="username"
            type="text"
            onChange={e => this.onInputChage(e)}
          />
          <Divider />
          <TextField
            hintText="Email"
            style={style}
            underlineShow={false}
            name="email"
            type="text"
            onChange={e => this.onInputChage(e)}
          />
          <Divider />
          <TextField
            hintText="Password"
            style={style}
            underlineShow={false}
            name="password"
            type="password"
            onChange={e => this.onInputChage(e)}
          />
          <Divider />
          <RaisedButton label="Register" fullWidth={true} onClick={this.register} />
        </Paper>
      </div>
    );
  }
}

export default graphql(register)(Register);
