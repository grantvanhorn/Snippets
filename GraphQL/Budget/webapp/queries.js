import gql from 'graphql-tag';

export const getBudget = gql`
query {
  getBudget {
    id
    name
  }
}
`;

export const allUsers = gql`
query {
  allUsers {
    id
    username
  }
}
`;
