import gql from 'graphql-tag';

export const register = gql`
mutation($username: String!, $password: String!, $email: String!) {
  register(username: $username, password: $password, email: $email) {
    id
    username
    email
  }
}
`;

export const login = gql`
mutation($username: String!, $password: String!) {
  login(username: $username, password: $password)
}
`;

export const createBudget = gql`
mutation($name: String!) {
  createBudget(name: $name) {
    id
    name
    allowance
  }
}
`;
