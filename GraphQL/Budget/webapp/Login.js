import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import history from '../history';

import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import { login } from '../mutations';
import './Login.css';

const style = {
  marginLeft: 20,
};

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      email: '',
      usernameLoginErrorText: '',
      passwordLoginErrorText: ''
    };
  }

  onInputChage = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  }

  login = async () => {
    try {
      const { data } = await this.props.mutate({ variables: this.state });
      const token = data.login;

      localStorage.setItem('token', token);
      history.push('/dashboard');
    } catch (e) {
      const error = e.graphQLErrors[0].message;

      if (error === 'No user with that username') {
        this.setState({
          ...this.state,
          usernameLoginErrorText: error,
          passwordLoginErrorText: ''
        })
      } else if (error === 'Incorrect password') {
        this.setState({
          ...this.state,
          usernameLoginErrorText: '',
          passwordLoginErrorText: error
        })
      } else {
        this.setState({
          ...this.state,
          usernameLoginErrorText: 'something went wrong',
          passwordLoginErrorText: ''
        })
      }
    }
  }

  render() {
    return (
      <div className="login-container">
        <Paper zDepth={2}>
          <Divider />
          <TextField
            hintText="Username"
            style={style}
            underlineShow={false}
            name="username"
            type="text"
            onChange={e => this.onInputChage(e)}
            errorText={this.state.usernameLoginErrorText}
          />
          <Divider />
          <TextField
            hintText="Password"
            style={style}
            underlineShow={false}
            name="password"
            type="password"
            onChange={e => this.onInputChage(e)}
            errorText={this.state.passwordLoginErrorText}
          />
          <Divider />
          <RaisedButton label="Login" fullWidth={true} onClick={this.login} />
          <Divider />
        </Paper>
      </div>
    );
  }
}

export default graphql(login)(Login);
