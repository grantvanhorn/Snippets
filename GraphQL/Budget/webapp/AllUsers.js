import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import { allUsers } from '../queries';

class AllUsers extends Component {

  // TODO : Figure out how to display this using async await or something better than checking
  // if the data loading attribute is false;
  showMutation() {
    const ready = !this.props.data.loading;
    const data = this.props.data.allUsers;

    if (ready) {
      const userList = data.map(user => {
        return <li key={user.id}>{user.id} | {user.username}</li>
      })

      return(
        <ul>{ userList }</ul>
      );
    }
  }

  render() {
    return (
      <div>
        All Users
        { this.showMutation() }
      </div>
    );
  }
}

export default graphql(allUsers)(AllUsers);
