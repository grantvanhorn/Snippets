export default `
  type User {
    id: Int!
    username: String!
    email: String!
    password: String!
    createdAt: String!
    updatedAt: String!
    budgets: [Budget!]!
  }

  type Budget {
    id: Int!
    name: String
    allowance: Int
    createdAt: String
    updatedAt: String
    ownerId: Int
    budgetCategories: [BudgetCategory]
  }

  type BudgetCategory {
    id: Int!
    budgetId: Int
    name: String
    amount: Int
    type: String
    description: String
  }

  input BudgetCategoryInput {
    name: String
    amount: Int
    type: String
    description: String
  }

  type Query {
    hello: String!
    allUsers: [User!]!
    me: User
    getUser(username: String!): User
    getBudget: Budget
  }

  type Mutation {
    register(username: String!, email: String!, password: String!): User!
    login(username: String!, password: String!): String!
    updateUser(username: String!, newUsername: String!): [Int!]!
    createBudget(name: String!, allowance: Int!): Budget!

    testCreateBudgetWithCategories(name: String!, categories: [BudgetCategoryInput]): Budget!
  }
`;
