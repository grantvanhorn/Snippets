import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import { requiresAuth, requiresAdmin} from './permissions';

export default {
  Query: {
    hello: (parent, args, context) => 'hi there test',
    allUsers: (parent, args, { models }) => models.User.findAll(),
    getUser: (parent, { username }, { models }) => models.User.findOne({ where: { username } }),
    getBudget: requiresAuth.createResolver(async (parent, args, { models, user }) => {
      console.log('user####', user)
      const budget = await models.Budget.findOne({ where: { UserId: user.id } })
      if(budget) {
        return budget
      } else {
        return null;
      }
    })
  },

  Mutation: {
    updateUser: (parent, { username, newUsername}, { models }) =>
      models.User.update({ username: newUsername}, { where: { username } }),
    // deleteUser: (parent, args, { models }) => models.User.destroy({ where: args }),
    register: async (parent, args, { models }) => {
      const user = args;
      user.password = await bcrypt.hash(user.password, 12);
      return models.User.create(user);
    },
    login: async (parent, { username, password }, { models, SECRET }) => {
      const user = await models.User.findOne({ where: { username } });
      if (!user) {
        throw new Error('No user with that username');
      }

      const valid = await bcrypt.compare(password, user.password);
      if (!valid) {
        throw new Error('Incorrect password');
      }

      const token = jwt.sign({ user: _.pick(user, ['id', 'username']) }, SECRET, { expiresIn: '1y' });

      return token;
    },
    createBudget: requiresAuth.createResolver((parent, args, { models, user }) => {
      return models.Budget.create({
        name: args.name,
        allowance: args.allowance,
        UserId: user.id
      });
    }),

    testCreateBudgetWithCategories: requiresAuth.createResolver( async (parent, args, {models, user}) => {
      const budget = await models.Budget.create({
        name: args.name,
        allowance: args.allowance,
        UserId: user.id
      });

      budget.budgetCategories = args.categories.map(async category => {
        return await models.BudgetCategory.create({
          BudgetId: budget.id,
          name: category.name,
          amount: category.amount,
          type: category.type,
          description: category.description
        });
      });

      return budget;
    })
  }
};
