import React, { Component } from 'react';

import './Clock.css';

class Clock extends Component {
  constructor(props) {
    super(props);

    let now = new Date();
    let military = this.dateToMilitaryTime(now);
    let standard = this.dateToStandardTime(now);

    this.state = {
      displayedFormat: 'standard',
      military,
      standard
    }
  }

  dateToStandardTime(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let diurnal = hours >= 12 ? 'PM' : 'AM';

    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;

    return { hours , minutes , diurnal }
  }

  dateToMilitaryTime(date) {
    return { hours: date.getHours(), minutes: ('0'+date.getMinutes()).slice(-2) }
  }

  setTime() {
    let now = new Date();

    let military = this.dateToMilitaryTime(now);
    let standard = this.dateToStandardTime(now);

    this.setState(
      {
        ...this.state,
        military,
        standard
      }
    );
  }

  componentDidMount() {
    window.setInterval(function () {
      this.setTime();
    }.bind(this), 1000);
  }

  displayTime() {
    return this.state.displayedFormat === 'standard' ? this.displayStandard() : this.displayMilitary();
  }

  displayStandard() {
    return `${this.state.standard.hours}:${this.state.standard.minutes} ${this.state.standard.diurnal}`;
  }

  displayMilitary() {
    return `${this.state.military.hours}:${this.state.military.minutes}`;
  }

  toggleFormat() {
    if (this.state.displayedFormat === 'standard')
      this.setState({ ...this.state, displayedFormat: 'military'});
    if (this.state.displayedFormat === 'military')
      this.setState({ ...this.state, displayedFormat: 'standard'});
  }

  render() {
    return (
      <div className="clock" onClick={() => this.toggleFormat()}>
        <i className="fa fa-clock-o" aria-hidden="true"></i>
        <span className="time-wrapper">
          { this.displayTime() }
        </span>
      </div>
    )
  }
}

export default Clock;

// CSS to go along with component.
// .clock {
//   cursor: default;
// }
//
// .clock i {
//   font-size: 20px;
// }
//
// .clock .time-wrapper {
//   padding-left: 5px;
//   font-size: 40px;
// }
