import React, { Component } from 'react';
import AddButtonSVG from './AddButtonSVG';
import { activityColors } from './lib';
import './ActivitySelector.scss';

export default class ActivitySelector extends Component {
  constructor() {
    super();
    this.state = {
      activities: [ ],
      inputActive: false,
      activityLabel: ''
    };
  }

  activityClassName = (activityId) => {
    const { selectedActivity: { id: selectedActivityId } } = this.props;
    return activityId === selectedActivityId ? 'activity selected' : 'activity';
  }

  activateInput = () => {
    this.setState({ inputActive: true });
  }

  submit = (e) => {
    e.preventDefault();
    const { selectActivity } = this.props;
    const { activities, activityLabel: label } = this.state;
    const color = activityColors[activities.length];
    const id = activities.length + 1;
    const newActivity = { label, color, id };

    this.setState({
      activities: [ ...activities, newActivity],
      inputActive: false,
      activityLabel: ''
    });

    selectActivity(newActivity);
  }

  renderInput = () => {
    const { inputActive, activityLabel } = this.state;

    if (inputActive) {
      return (
        <form className='label-form' onSubmit={this.submit}>
          <input
            className='form-input'
            value={activityLabel}
            onChange={(e) => this.setState({ activityLabel: e.target.value })}
            autoFocus
          />
        </form>
      );
    }
  }

  renderActivities = () => {
    const { activities } = this.state;
    const { selectActivity } = this.props;
    return activities.map(activity => {
      const { id, color, label } = activity;
      return (
        <div
          className={this.activityClassName(id)}
          key={id}
          onClick={() => selectActivity(activity)}
        >
          <div
            className='cell'
            style={{backgroundColor: color}}
          ></div>
          <div className='label'>
            {label}
          </div>
        </div>
      );
    });
  }

  renderAddButton() {
    const { activities } = this.state;
    const className = activities.length > 0 ? 'add-button-wrapper' : 'add-button-wrapper pulse';

    if (activities.length === activityColors.length) return null;

    return (
      <div
        className={className}
        onClick={this.activateInput}
      >
        <AddButtonSVG/>
      </div>
    );
  }

  render() {
    return (
      <div className='ActivitySelector'>
        {this.renderActivities()}
        {this.renderInput()}
        {this.renderAddButton()}
      </div>
    );
  }
}