import React, { Component } from 'react';
import { Chart } from "react-google-charts";
import { chartOptions } from './lib';

export default class ActivityChart extends Component {
  render() {
    const { data, colors } = this.props;

    return (
      <Chart
        chartType="PieChart"
        data={data}
        options={{ ...chartOptions, colors}}
        width="100%"
        height="500px"
      />
    );
  }
}