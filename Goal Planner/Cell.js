import React, { Component } from 'react';
import ReactDom from 'react-dom';
import './Cell.scss';

export default class Cell extends Component {
  constructor() {
    super();
    this.state = {
      activity: { }
    };
  }

  componentDidMount() {
    ReactDom.findDOMNode(this).addEventListener('mouseover', (e) => {
      if(e.buttons === 1 || e.buttons === 3){
          this.selected();
      }
    });
  }

  componentWillUnmount() {
    ReactDom.findDOMNode(this).addEventListener('mouseover');
  }

  selected = () => {
    const { activity } = this.state;
    const { selectCell, selectedActivity } = this.props;
    if (!selectedActivity) return;
    selectCell(selectedActivity, activity);
    this.setState({ activity: selectedActivity });
  }

  render() {
    const { activity: { color } } = this.state;
    return (
    <div
      className='Cell'
      onClick={() => this.selected()}
      style={ { backgroundColor:  color } }
      onMouseDown={() => this.selected()}
    ></div>
    );
  }
}