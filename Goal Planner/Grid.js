import React, { Component } from 'react';
import Cell from './Cell';
import { weekdayLabels, timeLabels } from './lib';
import './Grid.scss';

export default class Grid extends Component {
  renderDay = (dayNum) => {
    const { selectCell, selectedActivity } = this.props;
    const slots = 24;
    let cells = [];
    for ( var i = 0; i < slots; i++) {
      cells.push(
        <Cell
          key={`${dayNum}${i}`}
          selectCell={selectCell}
          selectedActivity={selectedActivity}
        />
      );
    }

    return (
      <div
        key={`day${dayNum}`}
        className='day'
      >
        {cells}
      </div>
    );
  }

  renderTimeLables() {
    const timeLablesDOM = timeLabels.map((label, i) => (
      <div
        key={`timelabel${i}`}
        className='time-label'
      >
        {label}
      </div>
    ));

    return (
      <div className='time-labels'>
        {timeLablesDOM}
      </div>
    );
  }

  renderWeek = () => {
    const numDays = 7;
    let days = [];

    for ( var i = 0; i < numDays; i++) {
      days.push(this.renderDay(i));
    }

    return (
      <div className='week'>
        {this.renderTimeLables()}
        {days}
        {this.renderTimeLables()}
      </div>
    );
  }

  renderWeekdayLabels = (side) => {
    const lables = weekdayLabels.map(label => (
      <div
        key={label}
        className={`label ${side}`}
      >
        {label}
      </div>
    ));

    return (
      <div className='weekday-labels'>
        {lables}
      </div>
    );
  }

  render() {
    return (
      <div className='Grid'>
        { this.renderWeekdayLabels('left') }
        { this.renderWeek() }
        { this.renderWeekdayLabels('right') }
      </div>
    );
  }
}