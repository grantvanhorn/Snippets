import React, { Component } from 'react';
import ActivityChart from './ActivityChart';
import ActivitySelector from './ActivitySelector';
import Grid from './Grid';
import './App.scss';

class App extends Component {
  constructor() {
    super();
    this.state = {
      selectedActivity: null,
      activityTracker: { }
    };
  }

  selectActivity = (selectedActivity) => {
    const { label, color } = selectedActivity;
    const { activityTracker } = this.state;
    const num = 0;

    if (!activityTracker[label]) {
      activityTracker[label] = { color, num };
    }

    this.setState({ selectedActivity, activityTracker });
  }

  isActivityEmpty(activity) {
    return (Object.keys(activity).length === 0);
  }

  selectCell = (newActivity, oldActivity) => {
    if (!this.isActivityEmpty(oldActivity)) {
      this.removeActivityFromTracker(oldActivity);
    }

    this.addActivityToTracker(newActivity);
  }

  removeActivityFromTracker(activity) {
    const { activityTracker } = this.state;
    activityTracker[activity.label].num--;
    this.setState({ activityTracker });
  }

  addActivityToTracker(activity) {
    const { activityTracker } = this.state;
    activityTracker[activity.label].num++;
    this.setState({ activityTracker });
  }

  activityChartData = () => {
    const { activityTracker } = this.state;
    const data = Object.keys(activityTracker).map(activity => [activity, activityTracker[activity].num]);
    const axisData = ['Activity', 'Hours'];
    const endData = [ axisData, ...data ];
    return endData;
  }

  colors() {
    const { activityTracker } = this.state;
    return Object.keys(activityTracker).map(activity => activityTracker[activity].color);
  }

  render() {
    const { selectedActivity } = this.state;
    return (
      <div className='App'>
        <div className='title'>
          Goal Planner
        </div>

        <div className='activity-selector-wrapper'>
          <ActivitySelector
            selectedActivity={selectedActivity}
            selectActivity={this.selectActivity}
          />
        </div>

        <Grid
          selectedActivity={selectedActivity}
          selectCell={this.selectCell}
        />

        <ActivityChart
          data={this.activityChartData()}
          colors={this.colors()}
        />
      </div>
    );
  }
}

export default App;
