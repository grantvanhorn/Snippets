export const chartOptions = {
  enableInteractivity: false,
  legend: 'none',
  backgroundColor: '#383838',
  pieSliceBorderColor: '#383838',
  pieSliceTextStyle: {
    color: '#383838',
  },
};

export const activityColors = [
  '#4253f4',
  '#41f4b5',
  '#dff441',
  '#ce1e4a',
  '#d67c24',
  '#9421ce'
];

export const weekdayLabels = [
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat'
];

export const timeLabels = [
  '12',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  '11',
  '12',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  '11',
];