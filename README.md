## Code Samples

Here are some code samples from some work I have done on the side purely for fun. Most of the projects never get finished. I use these
to learn about new technology. This helps me evaluate what tools to use in the future, up on best practices and seeing different things.

### Goal Planner

I want to focus on getting better at a couple of things, not many different things. So I created a simple block scheduling tool.

![alt text](https://media.giphy.com/media/7E8R8iyjDHWfSVsWVx/giphy.gif "Goal Planner")

A longer demo can be seen here since markdown does not support videos : https://gfycat.com/ShamefulWholeElephantseal

### Flash Cards

I want to learn Korean. I have played a lot of StarCraft in my life and used to follow the pro scene. Koreans are amazing at StarCraft and
this is how I was exposed to their culture. Learning the alphabet of Hangul is quite easy! I made a small React app to take me through
the characters and before a couple of rounds I was remembering quite a few!

### GraphQL

I am a huge fan of React so when I heard there was a server that goes along great with React I had to try it. They are not wrong, it was
so easy hooking GraphQL up to React. I really had a lot of fun with this one and hope to use it more. I took a very shallow dive into
GraphQL. It is on my list for sure and I will be in the deep end when time allows.

### Misc

Random code snippets.

### Screeps

Screeps is a game on Steam. A screep is a unit in the game. The whole game is played by writing JavaScript! It executes your code every
couple of seconds. You try to stay alive and build up against everyone else on the server. Check it out! Hosting a private server in the
office would be a great team activity! It would 100% kill productivity though.
