import React, { Component } from 'react';
import './FlashCard.css';
import 'font-awesome/css/font-awesome.min.css';

class FlashCard extends Component {
  constructor(props) {
    super(props);

    this.submitAnswer = this.submitAnswer.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  display() {
    return this.props.question ? this.props.question : this.props.answer;
  }

  handleChange(e) {
    this.props.handleChange(e);
  }

  submitAnswer(e) {
    this.props.checkAnswer(e);
  }

  questionStatusIcon() {
    if (this.props.status === 0) {
      return <i className="fa fa-minus-circle"></i>
    }

    else if (this.props.status === 1) {
      return <i className="fa fa-ban"></i>
    }

    else if (this.props.status === 2) {
      return <i className="fa fa-check-circle"></i>
    }
  }

  handleKeyPress(e) {
    if(e.key === 'Enter'){
      this.submitAnswer(e);
    }
  }

  render() {
    return (
      <div className="flash-card">
        <div className="question">{ this.display() }</div>

        <div className="status-icon">
          { this.questionStatusIcon() }
        </div>

        <div className="answer"><input onChange={this.handleChange} value={this.props.inputValue} onKeyPress={this.handleKeyPress}/></div>
        <div className="submit"><button onClick={this.submitAnswer}>check answer</button></div>
      </div>
    );
  }
}

export default FlashCard;
