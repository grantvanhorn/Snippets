import React, { Component } from 'react';
import './Stats.css';

class Stats extends Component {
  calculatePercentCorrect() {
    // TODO : Add case for NaN
    return (this.props.correct / this.props.attempts) * 100;
  }

  render() {
    return(
      <div>
        <i className="fa fa-percent"></i>
        { this.calculatePercentCorrect() }
      </div>
    );
  }

}

export default Stats;
