import React, { Component } from 'react';
import './SetPicker.css';

class SetPicker extends Component {
  displayQuestionSetPicker() {
    const setKeys = Object.keys(this.props.questionSet);
    return setKeys.map(key => {
      return <div
        className={key === this.props.questionMode ? 'select active' : 'select'}
        key={key}
        onClick={() => this.props.selectSet(key)}>{key}
      </div>
    });
  }

  render() {
    return(
      <div>
        <div>{ this.displayQuestionSetPicker() }</div>
      </div>
    );
  }
}

export default SetPicker;
