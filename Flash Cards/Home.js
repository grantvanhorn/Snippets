import React, { Component } from 'react';
import './Home.css';

import FlashCard from './FlashCard';
import Stats from './Stats';
import SetPicker from './SetPicker';

import { korean } from '../assets/questionSet';

class Home extends Component {

  constructor(props) {
    super(props);

    this.advance = this.advance.bind(this);
    this.checkAnswer = this.checkAnswer.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.selectSet = this.selectSet.bind(this);

    this.state = {
      questionSet: this.buildAllList(),
      currentQuestion: 0,
      status: 0,
      displayQuestion: true,
      answer: '',
      attempts: 0,
      correct: 0,
      questionMode: 'all'
    };
  }

  buildAllList() {
    let allList = [];
    for(let set in korean) {
      allList = allList.concat(korean[set]);
    }

    korean.all = allList;
    return korean;
  }

  checkAnswer(e) {
    e.stopPropagation();

    let answer = this.state.answer;
    let questionMode = this.state.questionMode;
    let currentQuestion = this.state.currentQuestion;
    let questionSetAnswer = this.state.questionSet[questionMode][currentQuestion].answer;;

    if(answer ===  questionSetAnswer) {
      this.setState({
        ...this.state,
        displayQuestion: false,
        status: 2,
        correct: this.state.correct + 1,
        attempts: this.state.attempts + 1
      });
    } else {
      this.setState({
        ...this.state,
        status: 1,
        attempts: this.state.attempts + 1
      });
    }
  }

  displayQuestionModeQuestions() {
    return this.state.questionSet[this.state.questionMode]
      .map(question => question.question);
  }

  selectSet(key) {
    this.setState({
      ...this.state,
      questionMode: key,
      currentQuestion: 0,
      status: 0,
      answer: ''
    });
  }

  handleChange(e) {
    this.setState({
      ...this.state,
      answer: e.target.value
    });
  }

  advance() {
    if (this.state.questionSet.length - 1 === this.state.currentQuestion) {
      this.setState({
        ...this.state,
        currentQuestion: 0,
        status: 0,
        answer: ''
      });
      return;
    }

    this.setState({
      ...this.state,
      currentQuestion: this.state.currentQuestion + 1,
      status: 0,
      answer: ''
    });
  }

  render() {
    const questionMode = this.state.questionMode;
    const currentQuestion = this.state.currentQuestion;

    return(
      <div className="home">
        <FlashCard
          question={this.state.questionSet[questionMode][currentQuestion].question}
          answer={this.state.questionSet[questionMode][currentQuestion].answer}
          checkAnswer={this.checkAnswer}
          status={this.state.status}
          handleChange={this.handleChange}
          inputValue={this.state.answer}
        />
        <div className="advance">
          <button onClick={this.advance}>advance</button>
        </div>

        <Stats
          attempts={this.state.attempts}
          correct={this.state.correct}
        />

        <SetPicker
          questionSet={this.state.questionSet}
          questionMode={this.state.questionMode}
          selectSet={this.selectSet}
        />
      </div>
    );
  }
}

export default Home;
